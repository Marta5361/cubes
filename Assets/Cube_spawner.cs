using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube_spawner : MonoBehaviour
{
    [SerializeField] GameObject cubePrefab;

    private void Awake()
    {
        int a = 0, b = 1, c = 0;
        var count = 20;
        for (int i = 0; i < count; i++)
        {
            c = a + b;
            var position = new Vector3(c, 0f, 0f);
            var renderer = Instantiate(cubePrefab, position, Quaternion.identity).GetComponent<MeshRenderer>();
            a = b;
            b = c;
        }
    }
}